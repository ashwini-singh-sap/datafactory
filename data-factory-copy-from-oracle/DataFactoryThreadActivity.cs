using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Rest;
using Microsoft.Extensions.Configuration;
using Microsoft.Azure.Management.ResourceManager;
using Microsoft.Azure.Management.DataFactory;
using Microsoft.Azure.Management.DataFactory.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Data;
using System.Text;
using System.Threading; 




namespace data_factory_copy_from_oracle
{

  

    class DataFactoryThreadActivity
    {


         private DataFactoryCopyActivityModel dfModel;

         private  KeyValuePair<string,List<ColumnDetails>> sourceTableAndColumnDetails;

         private KeyValuePair<string,List<ColumnDetails>> destTableAndColumnDetails;
         
         private DataFactoryManagementClient client;

         private ManualResetEvent _doneEvent;

        public DataFactoryThreadActivity(DataFactoryCopyActivityModel dfModel, KeyValuePair<string, List<ColumnDetails>> sourceTableAndColumnDetails, 
        KeyValuePair<string, List<ColumnDetails>> destTableAndColumnDetails, DataFactoryManagementClient client, ManualResetEvent doneEvent)
        {
            this.dfModel = dfModel;
            this.sourceTableAndColumnDetails = sourceTableAndColumnDetails;
            this.destTableAndColumnDetails = destTableAndColumnDetails;
            this.client = client;
           this._doneEvent = doneEvent;  
        }



        ///<summary>
        ///Gets the dictionary with all table names and for each table name all column details.
        ///</summary>
        public  void runThreadCopyActivity(Object threadContext)
        {  
               int threadIndex = (int)threadContext;  
               Console.WriteLine("thread {0} started...", threadIndex);
               DataFactoryCopyActivity dataFactoryCopyActivity = new DataFactoryCopyActivity();
            
                dataFactoryCopyActivity.createDataFactorySourceDataSet(dfModel, sourceTableAndColumnDetails, destTableAndColumnDetails, client);

                dataFactoryCopyActivity.createDataFactorySinkDataSet(dfModel, sourceTableAndColumnDetails, destTableAndColumnDetails, client);

                dataFactoryCopyActivity.createDataFactoryCopyPipeLine(dfModel, sourceTableAndColumnDetails, destTableAndColumnDetails, client);


                Console.WriteLine("Creating pipeline run for " + dfModel.SourceToAzureCopyDataPipelineName + "_" + sourceTableAndColumnDetails.Key + ", " + dfModel.SourceToAzureCopyActivityName + ", " + " zone2country1204sn  ...");

                Dictionary<string, object> parameters = new Dictionary<string, object>
                {
                    {"tableName", sourceTableAndColumnDetails.Key}
                };

                CreateRunResponse runResponse = client.Pipelines.CreateRunWithHttpMessagesAsync(dfModel.ResourceGroupName,
                dfModel.DataFactoryName, dfModel.SourceToAzureCopyDataPipelineName + "_" + sourceTableAndColumnDetails.Key, parameters).Result.Body;
                Console.WriteLine("Pipeline run ID: " + runResponse.RunId);

                // Monitor the pipeline run
                Console.WriteLine("Checking pipeline run status...");
                PipelineRun pipelineRun;
                while (true)
                {
                    System.Threading.Thread.Sleep(15000);
                    try
                    {
                        pipelineRun = client.PipelineRuns.Get(dfModel.ResourceGroupName, dfModel.DataFactoryName, runResponse.RunId);
                        Console.WriteLine("Status: " + pipelineRun.Status);
                        if (pipelineRun.Status == "InProgress")
                        {
                            // Do Nothing
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("pipeline run status : " + ex.Message);
                        if(ex.Message.Contains("Unauthorized",StringComparison.OrdinalIgnoreCase)){
                        client = Program.AuthenticateAndGetClient(dfModel);
                        }
                    } 
                }

                   // Check the copy activity run details
                Console.WriteLine("Checking activity '" + dfModel.SourceToAzureCopyActivityName + "' run details...");
                List<ActivityRun> activityRuns = client.ActivityRuns.ListByPipelineRun(
                  dfModel.ResourceGroupName, dfModel.DataFactoryName, runResponse.RunId, DateTime.UtcNow.AddMinutes(-10), DateTime.UtcNow.AddMinutes(10), "Succeeded", dfModel.SourceToAzureCopyActivityName).ToList();

                if (pipelineRun.Status == "Succeeded")
                {
                    ActivityRun myRun = activityRuns.First();
                    Console.WriteLine(activityRuns.First().Output);
                    client.Pipelines.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceToAzureCopyDataPipelineName + "_" + sourceTableAndColumnDetails.Key);
                    client.Datasets.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceDataSetname + "_" + sourceTableAndColumnDetails.Key);
                    client.Datasets.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SinkDataSetname + "_" + destTableAndColumnDetails.Key);

                }
                else
                {
                    Console.WriteLine(activityRuns.First().Error);
                }

                   Console.WriteLine("thread {0} finished...", threadIndex);  
                   _doneEvent.Set();
    
    
    }}

}