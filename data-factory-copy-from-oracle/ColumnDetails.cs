using System;

namespace data_factory_copy_from_oracle
{
    ///<summary>
    ///This is used to hold the information about one database column.
    ///</summary>
    public class ColumnDetails
    {
        ///<summary>
        ///The name of the column.
        ///</summary>
        public string columnName;
        ///<summary>
        ///The data type of the column.
        ///</summary>
        public string dataType;
        ///<summary>
        ///The max length (if available).
        ///</summary>
        public int maximumLength;

        ///<summary>
        ///Constructor with all elements.
        ///</summary>
        public ColumnDetails(string columnName, string dataType, int maximumLength)
        {
            this.columnName = columnName;
            this.dataType = dataType;
            this.maximumLength = maximumLength;
        }
        ///<summary>
        ///Constructor with no elements.
        ///</summary>
        public ColumnDetails()
        {

        }
    }
}