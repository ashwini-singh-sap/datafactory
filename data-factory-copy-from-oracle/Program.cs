﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Rest;
using Microsoft.Extensions.Configuration;
using Microsoft.Azure.Management.ResourceManager;
using Microsoft.Azure.Management.DataFactory;
using Microsoft.Azure.Management.DataFactory.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Runtime.Serialization.Json;

namespace data_factory_copy_from_oracle
{
    class Program
    {
        public static DataFactoryManagementClient client;
        DataFactoryCopyActivity dataFactoryCopyActivity;
        static void Main(string[] args)
        {
            Program myApp = new Program();
            DataFactoryCopyActivityModel dfModel = new DataFactoryCopyActivityModel();
            if (myApp.ReadConfig(dfModel))
            {
                myApp.ProcessCopy(dfModel);
            }
        }

        public void ProcessCopy(DataFactoryCopyActivityModel dfModel)
        {
            Console.WriteLine("Start Processing...");
            client = AuthenticateAndGetClient(dfModel);


            dataFactoryCopyActivity = new DataFactoryCopyActivity();

            Console.WriteLine("Getting data factory " + dfModel.DataFactoryName + "...");
            Factory dataFactory = client.Factories.Get(dfModel.ResourceGroupName, dfModel.DataFactoryName);
            Console.WriteLine(SafeJsonConvert.SerializeObject(dataFactory, client.SerializationSettings));

            //client.Pipelines.Delete(dfModel.ResourceGroupName,dfModel.DataFactoryName,dfModel.MySQLToAzureCopyDataPipelineName+"_zone2country1204sn");

            //client.Datasets.Delete(dfModel.ResourceGroupName,dfModel.DataFactoryName,dfModel.SourceDataSetname+"_zone2country1204sn");

            //client.Datasets.Delete(dfModel.ResourceGroupName,dfModel.DataFactoryName,dfModel.SinkDataSetname+"_zone2country1204sn");
            Dictionary<string, List<ColumnDetails>> sourceColumnDetails = null;

            if (dfModel.CacheTableColumnInfoToFile && System.IO.File.Exists(@"sourceColumnDetails.txt"))
            {
                try
                {
                    using (StreamReader sr = new StreamReader("sourceColumnDetails.txt"))
                    {
                        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Dictionary<string, List<ColumnDetails>>));
                        sourceColumnDetails = (Dictionary<string, List<ColumnDetails>>)ser.ReadObject(sr.BaseStream);
                        Console.WriteLine("Found this number of tables in cached file sourceColumnDetails.txt:" + sourceColumnDetails.Count);
                    }
                }
                catch (FileNotFoundException)
                {
                    Console.WriteLine("Column cache file sourceColumnDetails.txt not found.");
                }
            }

            if (sourceColumnDetails == null || sourceColumnDetails.Count == 0)
            {
                // Get Oracle Pipeline
                // Maybe not neccessary because later for the run we only use the name of the pipeline. 
                // But maybe it is good to see that the pipeline exists
                Console.WriteLine("Getting Pipeline " + dfModel.SourceTableAndColumnNamesPipelineName + "...");
                PipelineResource oracleTableAndColumnNamesPipeline = client.Pipelines.Get(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceTableAndColumnNamesPipelineName);
                Console.WriteLine(SafeJsonConvert.SerializeObject(oracleTableAndColumnNamesPipeline, client.SerializationSettings));

                // Run Oracle Pipeline and get Dictionary
                // List<string> oracleTableSelectors dfModel.TableSelectors.Split(',').ToList();
                sourceColumnDetails =
                dataFactoryCopyActivity.RetrieveTableAndColumnNames(dfModel, dfModel.SourceTableAndColumnNamesPipelineName,
                dfModel.LookupTableAndColumnNamesActivityName, dfModel.TableFilter, client);
                Console.WriteLine("Found this number of tables :" + sourceColumnDetails.Count);

                if (dfModel.CacheTableColumnInfoToFile)
                {
                    //open file stream
                    MemoryStream memStream = new MemoryStream();
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Dictionary<string, List<ColumnDetails>>));
                    ser.WriteObject(memStream, sourceColumnDetails);
                    memStream.Position = 0;
                    StreamReader sr = new StreamReader(memStream);
                    System.IO.File.WriteAllText(@"sourceColumnDetails.txt", sr.ReadToEnd());
                    Console.WriteLine("Source table and column info cached in sourceColumnDetails.txt.");
                }
            }


            Dictionary<string, List<ColumnDetails>> azureMsSqlColumnDetails = null;

            if (dfModel.CacheTableColumnInfoToFile && System.IO.File.Exists(@"azureMsSqlColumnDetails.txt"))
            {
                try
                {
                    using (StreamReader sr = new StreamReader("azureMsSqlColumnDetails.txt"))
                    {
                        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Dictionary<string, List<ColumnDetails>>));
                        azureMsSqlColumnDetails = (Dictionary<string, List<ColumnDetails>>)ser.ReadObject(sr.BaseStream);
                        Console.WriteLine("Found this number of tables in cached file azureMsSqlColumnDetails.txt:" + azureMsSqlColumnDetails.Count);
                    }
                }
                catch (FileNotFoundException)
                {
                    Console.WriteLine("Column cache file azureMsSqlColumnDetails.txt not found.");
                }
            }

            if (azureMsSqlColumnDetails == null || azureMsSqlColumnDetails.Count == 0)
            {

                // Get Azure Pipeline
                Console.WriteLine("Getting Pipeline " + dfModel.AzureMsSqlTableAndColumnNamesPipelineName + "...");
                PipelineResource azureMsSqlTableAndColumnNamesPipeline = client.Pipelines.Get(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.AzureMsSqlTableAndColumnNamesPipelineName);
                Console.WriteLine(SafeJsonConvert.SerializeObject(azureMsSqlTableAndColumnNamesPipeline, client.SerializationSettings));

                // Run Azure MS SQL Pipeline
                // List<string> azureMsSqlTableSelectors = dfModel.TableSelectors.Split(',').ToList();
                azureMsSqlColumnDetails = dataFactoryCopyActivity.RetrieveTableAndColumnNames(dfModel, dfModel.AzureMsSqlTableAndColumnNamesPipelineName,
                dfModel.LookupTableAndColumnNamesActivityName, dfModel.TableFilter, client);
                Console.WriteLine("Found this number of tables :" + azureMsSqlColumnDetails.Count);

                if (dfModel.CacheTableColumnInfoToFile)
                {
                    //open file stream
                    MemoryStream memStream = new MemoryStream();
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Dictionary<string, List<ColumnDetails>>));
                    ser.WriteObject(memStream, azureMsSqlColumnDetails);
                    memStream.Position = 0;
                    StreamReader sr = new StreamReader(memStream);
                    System.IO.File.WriteAllText(@"azureMsSqlColumnDetails.txt", sr.ReadToEnd());
                    Console.WriteLine("Source table and column info cached in azureMsSqlColumnDetails.txt.");
                }

            }

             // removing unmatched table
              removeUnwatedTableFromDestination(sourceColumnDetails , azureMsSqlColumnDetails);

            // Console.WriteLine("Ashwini : " +azureMsSqlColumnDetails);
            /* Create copy pipeline with copy activities inside, each copy activity has a column mapping and parallel
                    "enableStaging": false,
                    "parallelCopies": 4,
                    "cloudDataMovementUnits": 0
                run the copy activities in batch sizes of 20
            */
            // Here is an example how to create a pipeline with a copy activity
            // Create a pipeline with copy activity

            Console.WriteLine("getting pipeline " + dfModel.SourceToAzureCopyDataPipelineName + "...");
            //PipelineResource mySQLToAzureCopyDataPipelineResource = client.Pipelines.Get(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.MySQLToAzureCopyDataPipelineName);

            //Console.WriteLine(SafeJsonConvert.SerializeObject(mySQLToAzureCopyDataPipelineResource, client.SerializationSettings));

            dataFactoryCopyActivity.executeThreadCopyActivity(dfModel, dfModel.SourceToAzureCopyDataPipelineName,
            dfModel.SourceToAzureCopyActivityName, sourceColumnDetails, azureMsSqlColumnDetails, client);

        }

        ///<summary>
        ///Remove unwanted table from list
        ///</summary>
        void removeUnwatedTableFromDestination(Dictionary<string, List<ColumnDetails>> oracleColumnDetails
        , Dictionary<string, List<ColumnDetails>> azureMsSqlColumnDetails)
        {
            List<String> removeSourcelist = new List<string>();
            List<String> removeDestlist = new List<string>();
            foreach (KeyValuePair<string, List<ColumnDetails>> sourceTableColmn in oracleColumnDetails)
            {
                if (!azureMsSqlColumnDetails.ContainsKey(sourceTableColmn.Key))
                {
                    Console.WriteLine("Removing key from source : " + sourceTableColmn.Key);
                    removeSourcelist.Add(sourceTableColmn.Key);
                }
            }
            foreach (String sourceTable in removeSourcelist)
            {
                oracleColumnDetails.Remove(sourceTable);
            }

            foreach (KeyValuePair<string, List<ColumnDetails>> destTableColmn in azureMsSqlColumnDetails)
            {
                if (!oracleColumnDetails.ContainsKey(destTableColmn.Key))
                {
                    Console.WriteLine("Removing key from dest : " + destTableColmn.Key);
                    removeDestlist.Add(destTableColmn.Key);
                }
            }

            foreach (String destTable in removeDestlist)
            {
                azureMsSqlColumnDetails.Remove(destTable);
            }

            var sourceAndDest = oracleColumnDetails.Zip(azureMsSqlColumnDetails, (n, w) =>
                       new { sourceTableAndColumnNamesDictionary = n, destTableAndColumnNamesDictionary = w });

            foreach (var sourceAndDestList in sourceAndDest)
            {
                KeyValuePair<string, List<ColumnDetails>> sourceTableAndColumnDetails =
                                           sourceAndDestList.sourceTableAndColumnNamesDictionary;

                KeyValuePair<string, List<ColumnDetails>> destTableAndColumnDetails =
                sourceAndDestList.destTableAndColumnNamesDictionary;
                Console.WriteLine(sourceTableAndColumnDetails.Key + " : " + destTableAndColumnDetails.Key);

            }

        }

        ///<summary>
        ///Authenticates against Azure and retrieves a DataFactoryManagementClient.
        ///</summary>
        public static DataFactoryManagementClient AuthenticateAndGetClient(DataFactoryCopyActivityModel dfModel)
        {
            var context = new AuthenticationContext("https://login.windows.net/" + dfModel.TenantID);
            ClientCredential cc = new ClientCredential(dfModel.ApplicationId, dfModel.AuthenticationKey);
            AuthenticationResult result = context.AcquireTokenAsync("https://management.azure.com/", cc).Result;
            ServiceClientCredentials cred = new TokenCredentials(result.AccessToken);
            DataFactoryManagementClient client = new DataFactoryManagementClient(cred) { SubscriptionId = dfModel.SubscriptionId };
            return client;
        }


        Boolean ReadConfig(DataFactoryCopyActivityModel dfModel)
        {
            Config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("ashwini_appsettings.json")
                .Build();

            IConfigurationSection azureConfig = Config.GetSection("azureSettings");
            if (azureConfig.Exists())
            {
                dfModel.TenantID = azureConfig["tenantID"];
                dfModel.ApplicationId = azureConfig["applicationId"];
                dfModel.AuthenticationKey = azureConfig["authenticationKey"];
                dfModel.SubscriptionId = azureConfig["subscriptionId"];
                dfModel.ResourceGroupName = azureConfig["resourceGroupName"];
                dfModel.DataFactoryName = azureConfig["dataFactoryName"];

                dfModel.SourceTableAndColumnNamesPipelineName = azureConfig["sourceTableAndColumnNamesPipelineName"];
                dfModel.AzureMsSqlTableAndColumnNamesPipelineName = azureConfig["azureMsSqlTableAndColumnNamesPipelineName"];
                dfModel.LookupTableAndColumnNamesActivityName = azureConfig["lookupTableAndColumnNamesActivityName"];
                dfModel.SourceToAzureCopyDataPipelineName = azureConfig["sourceToAzureCopyDataPipelineName"];
                dfModel.SourceToAzureCopyActivityName = azureConfig["sourceToAzureCopyActivityName"];
                dfModel.SourceDataSetname = azureConfig["sourceDataSetname"];
                dfModel.SinkDataSetname = azureConfig["sinkDataSetname"];
                dfModel.SourceLinkedService = azureConfig["sourceLinkedService"];
                dfModel.SinkLinkedService = azureConfig["sinkLinkedService"];
                dfModel.TableFilter = azureConfig["tableFilter"];
                dfModel.UseCastQuery = bool.Parse(azureConfig["useCastQuery"]);
                dfModel.ExitAfterFailure = bool.Parse(azureConfig["exitAfterFailure"]);
                dfModel.CacheTableColumnInfoToFile = bool.Parse(azureConfig["cacheTableColumnInfoToFile"]);
                try
                {
                    dfModel.DataFactoryOutputLimit = int.Parse(azureConfig["DataFactoryOutputLimit"]);
                }
                catch (Exception)
                {
                    dfModel.DataFactoryOutputLimit = 5000;
                }
                try
                {
                    dfModel.ConcurrentPipelines = int.Parse(azureConfig["ConcurrentPipelines"]);
                }
                catch (Exception)
                {
                    dfModel.ConcurrentPipelines = 1;
                }
                if (dfModel.ConcurrentPipelines < 1)
                {
                    dfModel.ConcurrentPipelines = 1;
                }

                dfModel.SourceDataType = azureConfig["sourceDataType"];


                Dictionary<String, String> sourceDataTypeMapping = new Dictionary<String, String>();
                foreach (string splitDataType in azureConfig[azureConfig["useSourceMapping"]].Split(","))
                {
                    String[] dataMap = splitDataType.Split(":");
                    sourceDataTypeMapping.Add(dataMap[0], dataMap[1]);
                }

                foreach (KeyValuePair<string, string> splitDataType in sourceDataTypeMapping)
                {
                    Console.WriteLine(splitDataType.Key + ":" + splitDataType.Value);
                }

                dfModel.SourceDataTypeMapping = sourceDataTypeMapping;


                Dictionary<String, String> azureSqldataTypeMapping = new Dictionary<String, String>();
                foreach (string splitDataType in azureConfig["azureSqldataTypeMapping"].Split(","))
                {
                    String[] dataMap = splitDataType.Split(":");
                    azureSqldataTypeMapping.Add(dataMap[0], dataMap[1]);
                }

                foreach (KeyValuePair<string, string> splitDataType in azureSqldataTypeMapping)
                {
                    Console.WriteLine(splitDataType.Key + ":" + splitDataType.Value);
                }

                dfModel.AzureSqldataTypeMapping = azureSqldataTypeMapping;

                return true;
            }
            else
            {
                Console.WriteLine("No configuration found");
                return false;
            }
        }

        public static IConfigurationRoot Config { get; private set; }

    }
}
