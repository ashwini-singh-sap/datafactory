using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Rest;
using Microsoft.Extensions.Configuration;
using Microsoft.Azure.Management.ResourceManager;
using Microsoft.Azure.Management.DataFactory;
using Microsoft.Azure.Management.DataFactory.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Data;
using System.Text;
using System.Threading;




namespace data_factory_copy_from_oracle
{
    class DataFactoryCopyActivity
    {

        ///<summary>
        ///Gets the dictionary with all table names and for each table name all column details.
        ///</summary>
        /// <param name="dfModel"></param>
        /// <param name="pipelineName">The name of the pipeline.</param>
        /// <param name="activityName">The name of the lookup activity.</param>
        /// <param name="tableFilter">Optional filter for filtering tables
        ///   It completes the WHERE clause and is of the form " TABLE_NAME &lt; 'E'"</param>
        /// <param name="client"></param>
        public Dictionary<string, List<ColumnDetails>> RetrieveTableAndColumnNames(DataFactoryCopyActivityModel dfModel, string pipelineName,
         string activityName, string tableFilter, DataFactoryManagementClient client)
        {
            Dictionary<string, List<ColumnDetails>> tableAndColumnNamesDictionary = new Dictionary<string, List<ColumnDetails>>(StringComparer.InvariantCultureIgnoreCase);

            // counter for number of tables retrieved in the last pipeline run
            int columnsRetrieved = 0;

            // name of the last table retrieved, we use this to begin with in the next loop
            String lastTable = null;

            // optional table filter in the where statement
            if (tableFilter == null || tableFilter == "")
            {
                tableFilter = " 1=1 ";
            }

            // DataFactory limits output to 5000 lines so we need to loop until all tables and columns are retrieved
            do
            {

                // where to specify which table to begin with, plus the filter from the parameter
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                // if it's the first run
                if (lastTable == null)
                {
                    parameters["tableNameSelector"] = tableFilter;

                    // if it's not the first run, use the last table name from the previous run
                }
                else
                {
                    parameters["tableNameSelector"] = tableFilter + " AND TABLE_NAME >= '" + lastTable + "' ";
                }

                Console.WriteLine("Creating pipeline run for " + pipelineName + ", " + activityName + ", " + parameters["tableNameSelector"] + " ...");

                CreateRunResponse runResponse = client.Pipelines.CreateRunWithHttpMessagesAsync(dfModel.ResourceGroupName, dfModel.DataFactoryName, pipelineName, parameters).Result.Body;
                Console.WriteLine("Pipeline run ID: " + runResponse.RunId);

                Console.WriteLine(SafeJsonConvert.SerializeObject(runResponse, client.SerializationSettings));

                // Monitor the pipeline run
                Console.WriteLine("Checking pipeline run status...");
                PipelineRun pipelineRun;
                while (true)
                {
                    pipelineRun = client.PipelineRuns.Get(dfModel.ResourceGroupName, dfModel.DataFactoryName, runResponse.RunId);
                    Console.WriteLine("Status: " + pipelineRun.Status);
                    if (pipelineRun.Status == "InProgress")
                    {
                        System.Threading.Thread.Sleep(5000);
                    }
                    else
                    {
                        break;
                    }
                }

                // Check the copy activity run details
                Console.WriteLine("Checking activity '" + activityName + "' run details...");
                List<ActivityRun> activityRuns = client.ActivityRuns.ListByPipelineRun(
                    dfModel.ResourceGroupName, dfModel.DataFactoryName, runResponse.RunId, DateTime.UtcNow.AddMinutes(-10), DateTime.UtcNow.AddMinutes(10), "Succeeded", activityName).ToList();

                TableAndColumnNames tableAndColumnNames = null;
                if (pipelineRun.Status == "Succeeded")
                {
                    ActivityRun myRun = activityRuns.First();
                    // Console.WriteLine(activityRuns.First().Output);
                    tableAndColumnNames = SafeJsonConvert.DeserializeObject<TableAndColumnNames>(activityRuns.First().Output.ToString(), client.DeserializationSettings);
                }
                else
                {
                    Console.WriteLine(activityRuns.First().Error);
                }
                // Number of columns found in this run
                columnsRetrieved = tableAndColumnNames.count;
                Console.WriteLine("Number of columns found in this run: " + columnsRetrieved);

                // Last table name found in this run
                lastTable = (tableAndColumnNames.value.Last())["TABLE_NAME"];
                Console.WriteLine("Last table name found in this run: " + lastTable);

                // merging dictionaries
                AddToDictionary(tableAndColumnNames, ref tableAndColumnNamesDictionary);

                // remove last table if 5000 returned as it might be incomplete, we're beginning with the same table in the next loop
                if (columnsRetrieved == dfModel.DataFactoryOutputLimit)
                {
                    tableAndColumnNamesDictionary.Remove(lastTable);
                }

            } while (columnsRetrieved == dfModel.DataFactoryOutputLimit);

            return tableAndColumnNamesDictionary;
        }

        ///<summary>
        ///Gets the dictionary with all table names and for each table name all column details.
        ///</summary>
        /// <param name="tableAndColumnNames"></param>
        /// <param name="tableAndColumnNamesDictionary"></param>
        private void AddToDictionary(TableAndColumnNames tableAndColumnNames, ref Dictionary<string, List<ColumnDetails>> tableAndColumnNamesDictionary)
        {
            foreach (Dictionary<string, string> myValue in tableAndColumnNames.value)
            {

                string myTableName = "";
                string myColumnName = "";
                string myDataType = "";
                int myDataLength = -1;

                foreach (KeyValuePair<string, string> pair in myValue)
                {
                    //Console.WriteLine(pair.Key + " : " + pair.Value);
                    if ("TABLE_NAME".Equals(pair.Key))
                    {
                        myTableName = pair.Value;
                    }
                    else if ("COLUMN_NAME".Equals(pair.Key))
                    {
                        myColumnName = pair.Value;
                    }
                    else if ("DATA_TYPE".Equals(pair.Key))
                    {
                        myDataType = pair.Value;
                    }
                    else if ("DATA_LENGTH".Equals(pair.Key))
                    {
                        Int32.TryParse(pair.Value, out myDataLength);
                    }
                }
                ColumnDetails myColumnDetails = new ColumnDetails(myColumnName, myDataType, myDataLength);
                if (tableAndColumnNamesDictionary.ContainsKey(myTableName))
                {
                    tableAndColumnNamesDictionary[myTableName].Add(myColumnDetails);
                }
                else
                {
                    List<ColumnDetails> columnDetails = new List<ColumnDetails>();
                    columnDetails.Add(myColumnDetails);
                    tableAndColumnNamesDictionary.Add(myTableName, columnDetails);
                }
            }
        }


        ///<summary>
        ///Gets the dictionary with all table names and for each table name all column details.
        ///</summary>
        /// <param name="dfModel"></param>
        /// <param name="pipelineName"></param>
        /// <param name="activityName"></param>
        /// <param name="sourceTableAndColumnNamesDictionary"></param>
        /// <param name="destTableAndColumnNamesDictionary"></param>
        /// <param name="client"></param>
        public void executeCopyActivity(DataFactoryCopyActivityModel dfModel, string pipelineName, string activityName,
                                  Dictionary<string, List<ColumnDetails>> sourceTableAndColumnNamesDictionary,
                                  Dictionary<string, List<ColumnDetails>> destTableAndColumnNamesDictionary
                                  , DataFactoryManagementClient client)
        {

            var sourceAndDest = sourceTableAndColumnNamesDictionary.Zip(destTableAndColumnNamesDictionary, (n, w) => new { sourceTableAndColumnNamesDictionary = n, destTableAndColumnNamesDictionary = w });

            var sourceAndDestEnum = sourceAndDest.GetEnumerator();

            var activePipelines = new Dictionary<string, string>();

            bool allTablesProcessed = false;

            while (!allTablesProcessed)
            {

                while (activePipelines.Count < dfModel.ConcurrentPipelines && sourceAndDestEnum.MoveNext())
                {

                    var sourceAndDestList = sourceAndDestEnum.Current;

                    KeyValuePair<string, List<ColumnDetails>> sourceTableAndColumnDetails =
                    sourceAndDestList.sourceTableAndColumnNamesDictionary;

                    KeyValuePair<string, List<ColumnDetails>> destTableAndColumnDetails =
                    sourceAndDestList.destTableAndColumnNamesDictionary;

                    createDataFactorySourceDataSet(dfModel, sourceTableAndColumnDetails, destTableAndColumnDetails, client);

                    createDataFactorySinkDataSet(dfModel, sourceTableAndColumnDetails, destTableAndColumnDetails, client);

                    createDataFactoryCopyPipeLine(dfModel, sourceTableAndColumnDetails, destTableAndColumnDetails, client);

                    Console.WriteLine("Creating pipeline run for " + dfModel.SourceToAzureCopyDataPipelineName + "_" + sourceTableAndColumnDetails.Key + ", " + activityName);

                    Dictionary<string, object> parameters = new Dictionary<string, object>
                {
                    {"tableName", sourceTableAndColumnDetails.Key}
                };


                    bool exceptionThrown = false;
                    // retry if exception is thrown for too many connections
                    do
                    {
                        try
                        {
                            CreateRunResponse runResponse = client.Pipelines.CreateRunWithHttpMessagesAsync(dfModel.ResourceGroupName,
                            dfModel.DataFactoryName, dfModel.SourceToAzureCopyDataPipelineName + "_" + sourceTableAndColumnDetails.Key, parameters).Result.Body;
                            Console.WriteLine("Pipeline run ID for " + sourceTableAndColumnDetails.Key + ": " + runResponse.RunId);
                            activePipelines.Add(sourceTableAndColumnDetails.Key, runResponse.RunId);
                            exceptionThrown = false;
                        }
                        catch (ErrorResponseException ex)
                        {
                            exceptionThrown = true;
                            Console.WriteLine("Retrying because of Exception thrown: " + ex.Message);
                            System.Threading.Thread.Sleep(5000);
                        }
                    } while (exceptionThrown);


                }

                if (activePipelines.Count < dfModel.ConcurrentPipelines)
                {
                    allTablesProcessed = true;
                }

                // Monitor the pipeline runs
                Console.WriteLine("Checking pipeline run statuses...");
                PipelineRun pipelineRun;
                while (activePipelines.Count == dfModel.ConcurrentPipelines || allTablesProcessed && activePipelines.Count > 0)
                {
                    System.Threading.Thread.Sleep(5000);

                    var inactivePipelines = new Dictionary<string, string>();
                    foreach (KeyValuePair<string, string> activePipeline in activePipelines)
                    {
                        try
                        {
                            pipelineRun = client.PipelineRuns.Get(dfModel.ResourceGroupName, dfModel.DataFactoryName, activePipeline.Value);
                            Console.WriteLine("Status of pipeline " + activePipeline.Key + " (" + activePipeline.Value + ")" + pipelineRun.Status);
                            // if (pipelineRun.Status == "InProgress")
                            // Check the copy activity run details
                            Console.WriteLine("Checking activity '" + activityName + "' run details...");
                            List<ActivityRun> activityRuns = client.ActivityRuns.ListByPipelineRun(
                              dfModel.ResourceGroupName, dfModel.DataFactoryName, activePipeline.Value, DateTime.UtcNow.AddMinutes(-10), DateTime.UtcNow.AddMinutes(10), "Succeeded", activityName).ToList();

                            if (pipelineRun.Status == "Succeeded")
                            {
                                ActivityRun myRun = activityRuns.First();
                                // Console.WriteLine(activityRuns.First().Output);
                                client.Pipelines.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceToAzureCopyDataPipelineName + "_" + activePipeline.Key);
                                client.Datasets.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceDataSetname + "_" + activePipeline.Key);
                                client.Datasets.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SinkDataSetname + "_" + activePipeline.Key);
                                inactivePipelines.Add(activePipeline.Key, activePipeline.Value);
                            }
                            else if (pipelineRun.Status == "Failed")
                            {
                                if (dfModel.ExitAfterFailure)
                                {
                                    Console.WriteLine("Copy activity of " + activePipeline.Key + " failed. Cleaning up ... '");

                                    // cleanup before exiting after error
                                    foreach (KeyValuePair<string, string> deletePipeline in activePipelines)
                                    {
                                        Console.WriteLine("Deleting datasets and pipeline for " + deletePipeline.Key);
                                        client.Pipelines.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceToAzureCopyDataPipelineName + "_" + deletePipeline.Key);
                                        client.Datasets.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceDataSetname + "_" + deletePipeline.Key);
                                        client.Datasets.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SinkDataSetname + "_" + deletePipeline.Key);
                                    }
                                    Environment.Exit(-1);
                                }
                                else
                                {
                                    client.Pipelines.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceToAzureCopyDataPipelineName + "_" + activePipeline.Key);
                                    client.Datasets.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceDataSetname + "_" + activePipeline.Key);
                                    client.Datasets.Delete(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SinkDataSetname + "_" + activePipeline.Key);
                                    inactivePipelines.Add(activePipeline.Key, activePipeline.Value);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Pipeline run status exception: " + ex.Message);
                        }
                    }

                    // remove finished pipelines from activePipelines
                    foreach (KeyValuePair<string, string> pipeline in inactivePipelines)
                    {
                        activePipelines.Remove(pipeline.Key);
                    }

                }


                //  break;
            }
        }

        ///<summary>
        ///Gets the dictionary with all table names and for each table name all column details.
        ///</summary>
        /// <param name="dfModel"></param>
        /// <param name="pipelineName"></param>
        /// <param name="activityName"></param>
        /// <param name="sourceTableAndColumnNamesDictionary"></param>
        /// <param name="destTableAndColumnNamesDictionary"></param>
        /// <param name="client"></param>
        public void executeThreadCopyActivity(DataFactoryCopyActivityModel dfModel, string pipelineName, string activityName,
                                  Dictionary<string, List<ColumnDetails>> sourceTableAndColumnNamesDictionary,
                                  Dictionary<string, List<ColumnDetails>> destTableAndColumnNamesDictionary
                                  , DataFactoryManagementClient client)
        {

            var sourceAndDest = sourceTableAndColumnNamesDictionary.Zip(destTableAndColumnNamesDictionary, (n, w) => new { sourceTableAndColumnNamesDictionary = n, destTableAndColumnNamesDictionary = w });

                 int threadIndex = 0;
                    ThreadPool.SetMaxThreads(1,1);
                        // One event is used for each Fibonacci object.  

                ManualResetEvent[] doneEvents = new ManualResetEvent[dfModel.ConcurrentPipelines];  
                //DataFactoryThreadActivity[] fibArray = new DataFactoryThreadActivity[10]; 
               foreach (var sourceAndDestList in sourceAndDest)
                {
                KeyValuePair<string, List<ColumnDetails>> sourceTableAndColumnDetails =
                sourceAndDestList.sourceTableAndColumnNamesDictionary;

                KeyValuePair<string, List<ColumnDetails>> destTableAndColumnDetails =
                sourceAndDestList.destTableAndColumnNamesDictionary;
                
                 doneEvents[threadIndex] = new ManualResetEvent(false);


                   // Instantiating  DataFactoryThreadActivity class 
                DataFactoryThreadActivity dataFactoryThreadActivity = 
                new DataFactoryThreadActivity(dfModel, sourceTableAndColumnDetails, 
                destTableAndColumnDetails, client ,doneEvents[threadIndex]);

                   // Create a thread to execute the task, and then
                   // start the thread.
                   //Thread thread = new Thread(new ThreadStart(dataFactoryThreadActivity.runThreadCopyActivity));

                  ThreadPool.QueueUserWorkItem(
                      new WaitCallback(dataFactoryThreadActivity.runThreadCopyActivity),threadIndex);

                  // thread.Start();
                   Console.WriteLine("Main thread does some work, then waits.");
                  // thread.Join();
                  // Console.WriteLine( "Independent task has completed; main thread ends.");

                   ++threadIndex;

                   if(threadIndex % dfModel.ConcurrentPipelines == 0){
              // Wait for all threads in pool to calculate.

                 WaitHandle.WaitAll(doneEvents,500000);  
                 Console.WriteLine("All calculations are complete.");  
                 threadIndex = 0;
                 // break;
                   }
                 } 
                //  WaitHandle.WaitAll(doneEvents,500000);  
                //  Console.WriteLine("All calculations are complete.");
            
        }

        ///<summary>
        ///Gets the dictionary with all table names and for each table name all column details.
        ///</summary>
        /// <param name="dfModel"></param>
        /// <param name="sourceTableAndColumnDetails"></param>
        /// <param name="destTableAndColumnDetails"></param>
        /// <param name="client"></param>
        public void createDataFactoryCopyPipeLine(DataFactoryCopyActivityModel dfModel, KeyValuePair<string,
        List<ColumnDetails>> sourceTableAndColumnDetails,
        KeyValuePair<string,
        List<ColumnDetails>> destTableAndColumnDetails, DataFactoryManagementClient client)
        {

            Dictionary<string, object> columnMappings = new Dictionary<string, object>();
            // Console.WriteLine(SafeJsonConvert.SerializeObject(sourceTableAndColumnDetails.Value));

            StringBuilder sourceQuery = new StringBuilder("SELECT ");
            int i = 0;
            foreach (ColumnDetails columnDetails in sourceTableAndColumnDetails.Value)
            {
                if (i > 0)
                {
                    sourceQuery.Append(", ");
                }
                if(dfModel.SourceDataType == "mysql") {
                    if (columnDetails.dataType.Equals("nvarchar") || columnDetails.dataType.Equals("varchar")
                    || columnDetails.dataType.Equals("text") || columnDetails.dataType.Equals("longtext"))
                    {
                        sourceQuery.Append("CAST(" + columnDetails.columnName + " AS CHAR ) as " + columnDetails.columnName);
                    }
                    else
                    {
                        sourceQuery.Append(columnDetails.columnName);
                    }
                } else if(dfModel.SourceDataType == "hana") {
                    if (columnDetails.dataType.Equals("CLOB"))
                    {
                        sourceQuery.Append("CAST(" + columnDetails.columnName + " AS VARCHAR) as " + columnDetails.columnName);
                    }
                    else
                    {
                        sourceQuery.Append(columnDetails.columnName);
                    }
                }
                i++;
            }
            sourceQuery.Append(" FROM " + "hybris."+sourceTableAndColumnDetails.Key+ ";");

            String query = "SELECT * FROM " + sourceTableAndColumnDetails.Key;

            if (dfModel.UseCastQuery)
            {
                query = sourceQuery.ToString();
            }

            Console.WriteLine("Query : " + query);

            List<ColumnDetails> sourceTableAndColumnDetailsSorted = sourceTableAndColumnDetails.Value;

            List<ColumnDetails> destTableAndColumnDetailsSorted = destTableAndColumnDetails.Value;


            sourceTableAndColumnDetailsSorted.Sort(delegate (ColumnDetails x, ColumnDetails y)
              {
                  return x.columnName.CompareTo(y.columnName);
              });

            destTableAndColumnDetailsSorted.Sort(delegate (ColumnDetails x, ColumnDetails y)
            {
                return x.columnName.CompareTo(y.columnName);
            });

            var sourceAndDest = sourceTableAndColumnDetailsSorted.Zip(destTableAndColumnDetailsSorted, (n, w) => new { source = n, dest = w });

            foreach (var sourceAndDestColumn in sourceAndDest)
            {
                columnMappings.Add(sourceAndDestColumn.source.columnName, sourceAndDestColumn.dest.columnName);
            }
            // Create a pipeline with copy activity
            Console.WriteLine("Creating pipeline " + dfModel.SourceToAzureCopyDataPipelineName + "_" + sourceTableAndColumnDetails.Key + "...");

            PipelineResource pipeline = new PipelineResource
            {
                Activities = new List<Activity>
                {
                    new CopyActivity
                    {
                        Name = dfModel.SourceToAzureCopyActivityName,
                        Inputs = new List<DatasetReference>
                        {
                            new DatasetReference()
                            {
                                ReferenceName = dfModel.SourceDataSetname+"_"+sourceTableAndColumnDetails.Key
                            }
                        },
                        Outputs = new List<DatasetReference>
                        {
                            new DatasetReference
                            {
                                ReferenceName = dfModel.SinkDataSetname+"_"+destTableAndColumnDetails.Key
                            }
                        },
                        Source = new AzureMySqlSource  {
                            Query = query
                        } ,
                        Sink = new SqlSink {
                            WriteBatchSize = 10000,
                            PreCopyScript = new Expression("TRUNCATE TABLE [dbo].[@{pipeline().parameters.tableName}]")
                         },

                        Translator = new TabularTranslator(columnMappings)

                        //  AdditionalProperties = new Dictionary<string, Object>{
                        //             {"userProperties", "Ashwini" }
                        //          }
                    }
                },
                Parameters = new Dictionary<string, ParameterSpecification>{
                   {"tableName", new ParameterSpecification("String") }
                }
            };

            bool exceptionThrown = false;
            // retry if exception is thrown for too many connections
            do
            {
                try
                {
                    client.Pipelines.CreateOrUpdate(dfModel.ResourceGroupName, dfModel.DataFactoryName,
                    dfModel.SourceToAzureCopyDataPipelineName + "_" + sourceTableAndColumnDetails.Key, pipeline);
                    exceptionThrown = false;
                }
                catch (ErrorResponseException ex)
                {
                    exceptionThrown = true;
                    Console.WriteLine("Retrying because of Exception thrown: " + ex.Message);
                    System.Threading.Thread.Sleep(5000);
                    if(ex.Message.Contains("Unauthorized",StringComparison.OrdinalIgnoreCase))
                    {
                        client = Program.AuthenticateAndGetClient(dfModel);
                    }
                }
            } while (exceptionThrown);

            Console.WriteLine(SafeJsonConvert.SerializeObject(pipeline, client.SerializationSettings));

        }


        ///<summary>
        ///Gets the dictionary with all table names and for each table name all column details.
        ///</summary>
        /// <param name="dfModel"></param>
        /// <param name="sourceTableAndColumnDetails"></param>
        /// <param name="destTableAndColumnDetails"></param>
        /// <param name="client"></param>
        public void createDataFactorySourceDataSet(DataFactoryCopyActivityModel dfModel, KeyValuePair<string,
        List<ColumnDetails>> sourceTableAndColumnDetails,
         KeyValuePair<string,
        List<ColumnDetails>> destTableAndColumnDetails, DataFactoryManagementClient client)
        {
            // Create a Azure dataset
            List<DatasetDataElement> datasetDataElementStructure = new List<DatasetDataElement>();
            foreach (ColumnDetails columnDetails in sourceTableAndColumnDetails.Value)
            {
                datasetDataElementStructure.Add(new DatasetDataElement
                {
                    Name = columnDetails.columnName,
                    //Type = dfModel.SourceDataTypeMapping.ContainsKey(columnDetails.dataType) ? dfModel.SourceDataTypeMapping[columnDetails.dataType] : "Default"
                });
            }

            Console.WriteLine("Creating dataset " + dfModel.SourceDataSetname + "_" + sourceTableAndColumnDetails.Key + "...");
            DatasetResource sourceDataset = new DatasetResource(
                new RelationalTableDataset
                {
                    LinkedServiceName = new LinkedServiceReference
                    {
                        ReferenceName = dfModel.SourceLinkedService
                    },
                   // Structure = datasetDataElementStructure,

                    // TableName = sourceTableAndColumnDetails.Key
                }
            );

            if (dfModel.SourceDataType == "hana")
            {
                sourceDataset.Properties.Structure = datasetDataElementStructure;
            }


            bool exceptionThrown = false;
            // retry if exception is thrown for too many connections
            do
            {
                try
                {
                    client.Datasets.CreateOrUpdate(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceDataSetname + "_" + sourceTableAndColumnDetails.Key, sourceDataset);

                    DatasetResource datasetResource = client.Datasets.Get(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SourceDataSetname + "_" + sourceTableAndColumnDetails.Key);

                    exceptionThrown = false;
                }
                catch (ErrorResponseException ex)
                {
                    exceptionThrown = true;
                    Console.WriteLine("Retrying because of Exception thrown: " + ex.Message);
                    System.Threading.Thread.Sleep(5000);
                    if(ex.Message.Contains("Unauthorized",StringComparison.OrdinalIgnoreCase))
                    {
                    client = Program.AuthenticateAndGetClient(dfModel);
                    }
                }
            } while (exceptionThrown);

            // Console.WriteLine("Ashwini : " + SafeJsonConvert.SerializeObject(datasetResource.Properties.Structure, client.SerializationSettings));

            Console.WriteLine(SafeJsonConvert.SerializeObject(sourceDataset, client.SerializationSettings));

        }

        ///<summary>
        ///Gets the dictionary with all table names and for each table name all column details.
        ///</summary>
        /// <param name="dfModel"></param>
        /// <param name="sourceTableAndColumnDetails"></param>
        /// <param name="destTableAndColumnDetails"></param>
        /// <param name="client"></param>
        public void createDataFactorySinkDataSet(DataFactoryCopyActivityModel dfModel, KeyValuePair<string,
        List<ColumnDetails>> sourceTableAndColumnDetails,
        KeyValuePair<string,
        List<ColumnDetails>> destTableAndColumnDetails, DataFactoryManagementClient client)
        {
            // Create a Azure dataset
            List<DatasetDataElement> datasetDataElementStructure = new List<DatasetDataElement>();
            foreach (ColumnDetails columnDetails in destTableAndColumnDetails.Value)
            {
                datasetDataElementStructure.Add(new DatasetDataElement
                {
                    Name = columnDetails.columnName,
                    //Type = dfModel.AzureSqldataTypeMapping.ContainsKey(columnDetails.dataType) ? dfModel.AzureSqldataTypeMapping[columnDetails.dataType] : "Default"
                });
            }

            Console.WriteLine("Creating dataset " + dfModel.SinkDataSetname + "_" + destTableAndColumnDetails.Key + "...");
            DatasetResource sinkDataset = new DatasetResource(
                new AzureSqlTableDataset
                {
                    LinkedServiceName = new LinkedServiceReference
                    {
                        ReferenceName = dfModel.SinkLinkedService
                    },
                  //  Structure = datasetDataElementStructure,

                    TableName = destTableAndColumnDetails.Key
                }
            );

            if (dfModel.SourceDataType == "hana")
            {
                sinkDataset.Properties.Structure = datasetDataElementStructure;
            }

            bool exceptionThrown = false;

            // retry if exception is thrown for too many connections
            do
            {
                try
                {
                    client.Datasets.CreateOrUpdate(dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SinkDataSetname + "_" + destTableAndColumnDetails.Key, sinkDataset);
                    DatasetsOperationsExtensions.CreateOrUpdate(client.Datasets, dfModel.ResourceGroupName, dfModel.DataFactoryName, dfModel.SinkDataSetname + "_" + destTableAndColumnDetails.Key, sinkDataset);
                    exceptionThrown = false;
                }
                catch (ErrorResponseException ex)
                {
                    exceptionThrown = true;
                    Console.WriteLine("Retrying because of Exception thrown: " + ex.Message);
                    System.Threading.Thread.Sleep(5000);
                    if(ex.Message.Contains("Unauthorized",StringComparison.OrdinalIgnoreCase))
                    {
                    client = Program.AuthenticateAndGetClient(dfModel);
                    }
                }
            } while (exceptionThrown);
            Console.WriteLine(SafeJsonConvert.SerializeObject(sinkDataset, client.SerializationSettings));

        }
    }
}
