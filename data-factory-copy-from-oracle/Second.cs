using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Rest;
using Microsoft.Azure.Management.ResourceManager;
using Microsoft.Azure.Management.DataFactory;
using Microsoft.Azure.Management.DataFactory.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace data_factory_copy_from_oracle
{
    class Program2
    {
        // Set variables
        // for azrspn_hyb_cc14
        string tenantID = "42f7676c-f455-423c-82f6-dc2d99791af7";
        string applicationId = "f27666a4-8298-4ec6-b475-72cef0e56678";
        string authenticationKey = "Aqmivk0xDxvupodrExGk9ONl0KfB2P5dWLS9DneEUl0=";
        // for TomsDataFactory
        //string tenantID = "42f7676c-f455-423c-82f6-dc2d99791af7"; // SERVICE IDENTITY TENANT
        //string applicationId = "b6dc10d6-edb2-4fc4-9d3f-9e5ee4942fbb"; // SERVICE IDENTITY APPLICATION ID
        //string authenticationKey = "128fa58e-2fc0-42c2-bc27-4230c6f453ec"; // SERVICE IDENTITY ID

        string subscriptionId = "4cd75adf-fc28-49fd-8152-06f9da6c15e1"; // From [MT P4] Hybris Commerce Cloud customer - Pilot 4
        string resourceGroupName = "esdef-esdefsus-slytherin";
        //string region = "West Europe";
        string dataFactoryName = "TomsDataFactory";
        string listTablenamesOraclePipelineName = "GetTableNamesOracle"; // GetTableAndColumnNamesOracle
        string listTablenamesAzureMSSQLPipelineName = "GetTableNamesAzureMsSQL2";
        //string simpleCopyPipelineName = "tomsSimpleCopy";

        //string storageAccount = "tomsblob4media";
        //string storageKey = "IGhuG8yswKWv7HTdZSNvcALehkBDywnGVkdWdnd0K3y579jvS+roLOMgwv9mfJb+DMnDBdY1lhNYwg0IRbVyeQ==";
        //string inputBlobPath = "toms-media/media";
        //string outputBlobPath = "toms-media2";

        //string storageLinkedServiceName = "tomsAzureStorageLinkedService";  // name of the Azure Storage linked service
        //string blobDatasetName = "tomsBlobDataset";             // name of the blob dataset
        //string pipelineName = "tomsAdfv2QuickStartPipeline";    // name of the pipeline

        DataFactoryManagementClient client;
        static void Hugo(string[] args){
            Program2 myApp = new Program2();
            myApp.ProcessCopy();
        }

        public void ProcessCopy() {
            // Authenticate and create a data factory management client
            var context = new AuthenticationContext("https://login.windows.net/" + tenantID);
            ClientCredential cc = new ClientCredential(applicationId, authenticationKey);
            AuthenticationResult result = context.AcquireTokenAsync("https://management.azure.com/", cc).Result;
            ServiceClientCredentials cred = new TokenCredentials(result.AccessToken);
            //var client = new DataFactoryManagementClient(cred) { SubscriptionId = subscriptionId };
            client = new DataFactoryManagementClient(cred);
            client.SubscriptionId = subscriptionId;

            Console.WriteLine("Getting data factory " + dataFactoryName + "...");
            Factory dataFactory = client.Factories.Get(resourceGroupName, dataFactoryName);
            // Create a data factory
            //Console.WriteLine("Creating data factory " + dataFactoryName + "...");
            //Factory dataFactory = new Factory
            //{
            //    Location = region,
            //    Identity = new FactoryIdentity()
            //};
            //client.Factories.CreateOrUpdate(resourceGroup, dataFactoryName, dataFactory);
            Console.WriteLine(SafeJsonConvert.SerializeObject(dataFactory, client.SerializationSettings));

            // while (client.Factories.Get(resourceGroup, dataFactoryName).ProvisioningState == "PendingCreation")
            //{
            //    System.Threading.Thread.Sleep(1000);
            //}

            PipelineResource listTablenamesOraclePipeline = client.Pipelines.Get(resourceGroupName, dataFactoryName, listTablenamesOraclePipelineName);
            Console.WriteLine("The Pipeline " + listTablenamesOraclePipelineName + " is");
            Console.WriteLine(SafeJsonConvert.SerializeObject(listTablenamesOraclePipeline, client.SerializationSettings));

            PipelineResource listTablenamesAzureMSSQLPipeline = client.Pipelines.Get(resourceGroupName, dataFactoryName, listTablenamesAzureMSSQLPipelineName);
            Console.WriteLine("The Pipeline " + listTablenamesAzureMSSQLPipelineName + " is");
            Console.WriteLine(SafeJsonConvert.SerializeObject(listTablenamesAzureMSSQLPipeline, client.SerializationSettings));

            /* string tomsSimpleCopyPipelineName = "tomsSimpleCopy";
            PipelineResource tomsSimpleCopyPipeline = client.Pipelines.Get(resourceGroupName, dataFactoryName, tomsSimpleCopyPipelineName);
            Console.WriteLine("The Pipeline " + tomsSimpleCopyPipelineName + " is");
            Console.WriteLine(SafeJsonConvert.SerializeObject(tomsSimpleCopyPipeline, client.SerializationSettings));
 */
            // Create an Azure Storage linked service
            /* Console.WriteLine("Creating linked service " + storageLinkedServiceName + "...");

            LinkedServiceResource storageLinkedService = new LinkedServiceResource(
                new AzureStorageLinkedService
                {
                    ConnectionString = new SecureString("DefaultEndpointsProtocol=https;AccountName=" + storageAccount + ";AccountKey=" + storageKey)
                }
            );
            client.LinkedServices.CreateOrUpdate(resourceGroup, dataFactoryName, storageLinkedServiceName, storageLinkedService);
            Console.WriteLine(SafeJsonConvert.SerializeObject(storageLinkedService, client.SerializationSettings));
 */
            // Create a Azure Blob dataset
/*             Console.WriteLine("Creating dataset " + blobDatasetName + "...");
            DatasetResource blobDataset = new DatasetResource(
                new AzureBlobDataset
                {
                    LinkedServiceName = new LinkedServiceReference
                    {
                        ReferenceName = storageLinkedServiceName
                    },
                    FolderPath = new Expression { Value = "@{dataset().path}" },
                    Parameters = new Dictionary<string, ParameterSpecification>
                    {
            { "path", new ParameterSpecification { Type = ParameterType.String } }

                    }
                }
            );
            client.Datasets.CreateOrUpdate(resourceGroup, dataFactoryName, blobDatasetName, blobDataset);
            Console.WriteLine(SafeJsonConvert.SerializeObject(blobDataset, client.SerializationSettings));
 */
            // Create a pipeline with copy activity
/*             Console.WriteLine("Creating pipeline " + pipelineName + "...");
            PipelineResource pipeline = new PipelineResource
            {
                Parameters = new Dictionary<string, ParameterSpecification>
    {
        { "inputPath", new ParameterSpecification { Type = ParameterType.String } },
        { "outputPath", new ParameterSpecification { Type = ParameterType.String } }
    },
                Activities = new List<Activity>
    {
        new CopyActivity
        {
            Name = "CopyFromBlobToBlob",
            Inputs = new List<DatasetReference>
            {
                new DatasetReference()
                {
                    ReferenceName = blobDatasetName,
                    Parameters = new Dictionary<string, object>
                    {
                        { "path", "@pipeline().parameters.inputPath" }
                    }
                }
            },
            Outputs = new List<DatasetReference>
            {
                new DatasetReference
                {
                    ReferenceName = blobDatasetName,
                    Parameters = new Dictionary<string, object>
                    {
                        { "path", "@pipeline().parameters.outputPath" }
                    }
                }
            },
            Source = new BlobSource { },
            Sink = new BlobSink { }
        }
    }
            };
            client.Pipelines.CreateOrUpdate(resourceGroup, dataFactoryName, pipelineName, pipeline);
            Console.WriteLine(SafeJsonConvert.SerializeObject(pipeline, client.SerializationSettings));
 */
            TableAndColumnNames tableNamesOracle = RetrieveTableNames(listTablenamesOraclePipelineName, "LookupTableNames");
            TableAndColumnNames tableNamesAzureMSSQL = RetrieveTableNames(listTablenamesAzureMSSQLPipelineName, "LookupTableNames");

            if (tableNamesOracle.count != tableNamesAzureMSSQL.count) {
                Console.WriteLine("ups different number of tables - " + tableNamesOracle.count + " vs. " + tableNamesAzureMSSQL.count);
            } else {
                Console.WriteLine("Number of tables matching");
            }

            Dictionary<string, List<string>> columnNamesOracle = RetrieveColumnNames(tableNamesOracle);
            Dictionary<string, List<string>> columnNamesAzureMsSQL = RetrieveColumnNames(tableNamesAzureMSSQL);


            //Console.WriteLine("Deleting the data factory");
            //client.Factories.Delete(resourceGroup, dataFactoryName);
            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey();
        }

        public TableAndColumnNames RetrieveTableNames(string pipelineName, string activityName) {
            // Create a pipeline run
            Console.WriteLine("Creating pipeline run for " + pipelineName + " ...");
            /*             Dictionary<string, object> parameters = new Dictionary<string, object>
                        {
                            { "inputPath", inputBlobPath },
                            { "outputPath", outputBlobPath }
                        };
            */
            //CreateRunResponse runResponse = client.Pipelines.CreateRunWithHttpMessagesAsync(resourceGroup, dataFactoryName, pipelineName, parameters).Result.Body;
            CreateRunResponse runResponse = client.Pipelines.CreateRunWithHttpMessagesAsync(resourceGroupName, dataFactoryName, pipelineName).Result.Body;
            Console.WriteLine("Pipeline run ID: " + runResponse.RunId);

            // Monitor the pipeline run
            Console.WriteLine("Checking pipeline run status...");
            PipelineRun pipelineRun;
            while (true)
            {
                pipelineRun = client.PipelineRuns.Get(this.resourceGroupName, dataFactoryName, runResponse.RunId);
                Console.WriteLine("Status: " + pipelineRun.Status);
                if (pipelineRun.Status == "InProgress")
                    System.Threading.Thread.Sleep(15000);
                else
                    break;
            }

            // Check the copy activity run details
            Console.WriteLine("Checking activity '" + activityName + "' run details...");
            List<ActivityRun> activityRuns;
            while(true) {
                activityRuns = client.ActivityRuns.ListByPipelineRun(
                resourceGroupName, dataFactoryName, runResponse.RunId, DateTime.UtcNow.AddMinutes(-10), DateTime.UtcNow.AddMinutes(10), "Succeeded", activityName).ToList();
                if (!activityRuns.Any()) {
                    System.Threading.Thread.Sleep(15000);
                    Console.WriteLine("No results yet ...");
                } else {
                    break;
                }
            }
             
            TableAndColumnNames tableNames = null;
            if (pipelineRun.Status == "Succeeded") {
                ActivityRun myRun = activityRuns.First();
                 
                Console.WriteLine(activityRuns.First().Output);
                tableNames = SafeJsonConvert.DeserializeObject<TableAndColumnNames>(activityRuns.First().Output.ToString(), client.DeserializationSettings);
                Console.WriteLine("Number of tables found : " + tableNames.count);
            } else {
                Console.WriteLine(activityRuns.First().Error);
            }
            return tableNames;
        }

        Dictionary<string, List<string>> RetrieveColumnNames(TableAndColumnNames tableNames) {
            Dictionary<string, List<string>> allColumnNames = new Dictionary<string, List<string>>();
            foreach (Dictionary<string,string> myValue in tableNames.value) {
                foreach (KeyValuePair<string,string> pair in myValue) {
                    Console.WriteLine(pair.Key + " : " + pair.Value);
                    if ("TABLE_NAME".Equals(pair.Key)) {
                        string myTableName = pair.Value;
                        // call pipeline to get Oracle column names
                        string listColumnNamesOraclePipelineName = "GetColumnNamesOracle";
                        PipelineResource listColumnNamesOraclePipeline = client.Pipelines.Get(resourceGroupName, dataFactoryName, listColumnNamesOraclePipelineName);
                        Console.WriteLine("The Pipeline " + listColumnNamesOraclePipelineName + " is");
                        Console.WriteLine(SafeJsonConvert.SerializeObject(listColumnNamesOraclePipeline, client.SerializationSettings));
                        // run the pipeline
                        Dictionary<string, object> parameters = new Dictionary<string, object>
                        {
                            { "tableName", myTableName }
                        };
                        CreateRunResponse runResponse = client.Pipelines.CreateRunWithHttpMessagesAsync(resourceGroupName, dataFactoryName, listColumnNamesOraclePipelineName, parameters).Result.Body;
                        Console.WriteLine("Pipeline run ID: " + runResponse.RunId);

                        // Monitor the pipeline run
                        Console.WriteLine("Checking pipeline run status...");
                        PipelineRun pipelineRun;
                        while (true) {
                            pipelineRun = client.PipelineRuns.Get(this.resourceGroupName, dataFactoryName, runResponse.RunId);
                            Console.WriteLine("Status: " + pipelineRun.Status);
                            if (pipelineRun.Status == "InProgress") {
                                System.Threading.Thread.Sleep(15000);
                            } else {
                                break;
                            }
                        }
                        List<ActivityRun> activityRuns = client.ActivityRuns.ListByPipelineRun(
                            resourceGroupName, dataFactoryName, runResponse.RunId, DateTime.UtcNow.AddMinutes(-10), DateTime.UtcNow.AddMinutes(10), "Succeeded", "LookupColumnNames").ToList();
                        TableAndColumnNames columnNames = null;
                        if (pipelineRun.Status == "Succeeded") {
                            ActivityRun myRun = activityRuns.First();
                            
                            Console.WriteLine(activityRuns.First().Output);
                            columnNames = SafeJsonConvert.DeserializeObject<TableAndColumnNames>(activityRuns.First().Output.ToString(), client.DeserializationSettings);
                            Console.WriteLine("Number of columns found : " + columnNames.count);
                            List<string> columnNamesList = new List<string>();
                            foreach (Dictionary<string,string> myColumn in columnNames.value)
                            {
                                foreach (KeyValuePair<string,string> myColumnPair in myColumn)
                                {
                                    if ("COLUMN_NAME".Equals(myColumnPair.Key)) {
                                        columnNamesList.Add(myColumnPair.Value);
                                    }
                                }
                            }
                            allColumnNames.Add(myTableName, columnNamesList);
                        } else {
                            Console.WriteLine(activityRuns.First().Error);
                        }
                    }
                }
            }
            return allColumnNames;    
        }
    }
}
