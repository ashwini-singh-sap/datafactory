using System;
using System.Collections.Generic;

namespace data_factory_copy_from_oracle
{
    ///<summary>
    ///This is used to hold the transformed JSON result from
    ///the pipeline run that gets table and column names.
    ///</summary>
    public class TableAndColumnNames
    {
        ///<summary>
        ///The number of objects returned.
        ///</summary>
        public int count;
        ///<summary>
        ///The list with the details for each column as key,value.
        ///</summary>
        public List<Dictionary<string,string>> value;
        ///<summary>
        ///The used Integration Runtime.
        ///</summary>
        public string effectiveIntegrationRuntime;
    }
}