using System.Collections.Generic;

namespace data_factory_copy_from_oracle
{
    internal class DataFactoryCopyActivityModel
    {
        string tenantID;
        string applicationId;
        string authenticationKey;
        string subscriptionId;
        string resourceGroupName;
        string dataFactoryName;
        string sourceTableAndColumnNamesPipelineName;
        string azureMsSqlTableAndColumnNamesPipelineName;
        string lookupTableAndColumnNamesActivityName;
        string sourceToAzureCopyDataPipelineName;

        string sourceToAzureCopyActivityName;

        string sourceDataSetname;

        string sinkDataSetname;

        string sourceLinkedService;

        string sinkLinkedService;

        string tableFilter;
        Dictionary<string, string> sourceDataTypeMapping;
        Dictionary<string, string> azureSqldataTypeMapping;

        string sourceDataType;

        int concurrentPipelines;

        bool useCastQuery;

        bool exitAfterFailure;

        bool cacheTableColumnInfoToFile;

        int dataFactoryOutputLimit;

        public string TenantID { get => tenantID; set => tenantID = value; }
        public string ApplicationId { get => applicationId; set => applicationId = value; }
        public string AuthenticationKey { get => authenticationKey; set => authenticationKey = value; }
        public string SubscriptionId { get => subscriptionId; set => subscriptionId = value; }
        public string ResourceGroupName { get => resourceGroupName; set => resourceGroupName = value; }
        public string DataFactoryName { get => dataFactoryName; set => dataFactoryName = value; }
        public string SourceTableAndColumnNamesPipelineName { get => sourceTableAndColumnNamesPipelineName; set => sourceTableAndColumnNamesPipelineName = value; }
        public string AzureMsSqlTableAndColumnNamesPipelineName { get => azureMsSqlTableAndColumnNamesPipelineName; set => azureMsSqlTableAndColumnNamesPipelineName = value; }
        public string LookupTableAndColumnNamesActivityName { get => lookupTableAndColumnNamesActivityName; set => lookupTableAndColumnNamesActivityName = value; }
        public string SourceToAzureCopyDataPipelineName { get => sourceToAzureCopyDataPipelineName; set => sourceToAzureCopyDataPipelineName = value; }
        public string SourceToAzureCopyActivityName { get => sourceToAzureCopyActivityName; set => sourceToAzureCopyActivityName = value; }
        public string SourceDataSetname { get => sourceDataSetname; set => sourceDataSetname = value; }
        public string SinkDataSetname { get => sinkDataSetname; set => sinkDataSetname = value; }

        public string SourceLinkedService { get => sourceLinkedService; set => sourceLinkedService = value; }
        public string SinkLinkedService { get => sinkLinkedService; set => sinkLinkedService = value; }

        public string TableFilter { get => tableFilter; set => tableFilter = value; }
        public Dictionary<string, string> SourceDataTypeMapping { get => sourceDataTypeMapping; set => sourceDataTypeMapping = value; }
        public Dictionary<string, string> AzureSqldataTypeMapping { get => azureSqldataTypeMapping; set => azureSqldataTypeMapping = value; }
        public string SourceDataType { get => sourceDataType; set => sourceDataType = value; }

        public int ConcurrentPipelines { get => concurrentPipelines; set => concurrentPipelines = value; }

        public bool UseCastQuery { get => useCastQuery; set => useCastQuery = value; }
        public bool ExitAfterFailure { get => exitAfterFailure; set => exitAfterFailure = value; }
        public bool CacheTableColumnInfoToFile { get => cacheTableColumnInfoToFile; set => cacheTableColumnInfoToFile = value; }


        public int DataFactoryOutputLimit { get => dataFactoryOutputLimit; set => dataFactoryOutputLimit = value; }

    }
}